#include "widget.h"
#include "global.h"
#include <QApplication>
#include <QTextCodec>
#include <QStyleFactory>
#include <QStyle>

QNetworkAccessManager * GlobalManager ::netw = nullptr;
QString GlobalManager::currentLogin = "Guest";
QTextCodec * GlobalManager::codec = QTextCodec::codecForName("Windows-1251");

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setStyle("Fusion");

//USE FOR TESTING:
//username: Esperanto
//password: password
    Widget w;
    w.show();
    //DONE: Change architecture
    return a.exec();
}
