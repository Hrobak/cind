#-------------------------------------------------
#
# Project created by QtCreator 2013-09-01T10:29:10
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = contesterInd
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    viewer.cpp \
    sendsolution.cpp \
    gototask.cpp \
    tasklist.cpp \
    mysubmissions.cpp \
    highlighter.cpp \
    solutionviewer.cpp

HEADERS  += widget.h \
    global.h \
    viewer.h \
    sendsolution.h \
    gototask.h \
    tasklist.h \
    mysubmissions.h \
    highlighter.h \
    solutionviewer.h

OTHER_FILES += \
    temp.txt \
    logFormat.txt

FORMS += \
    viewer.ui \
    sendsolution.ui \
    gototask.ui
QMAKE_CXXFLAGS += -std=c++11

RESOURCES += \
    images.qrc
