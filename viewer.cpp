#include "viewer.h"
#include "ui_viewer.h"
#include "global.h"
#include "sendsolution.h"
#include "gototask.h"
#include "tasklist.h"
#include "mysubmissions.h"
#include <iostream>
Viewer::Viewer(const QString &pass, QDialog *parent) :
    QDialog(parent),
    ui(new Ui::Viewer),
    currentPass (pass),
    showedData(nullptr)
{
    ui->setupUi(this);
    ui->menuOptions->addItem(tr("Submit solution"));
    ui->menuOptions->addItem(tr("Go to task"));
    ui->menuOptions->addItem(tr("Task list"));
    ui->menuOptions->addItem(tr("My submissions"));

    ui->userName->setText(GlobalManager::currentLogin);
    mainLayout = new QVBoxLayout;
    headerLayout = new QHBoxLayout;
    headerLayout->addWidget(ui->menuOptions);
    headerLayout->addWidget(ui->enteredAs);

    headerLayout->addWidget(ui->userName);
    headerLayout->addWidget(ui->logOffButton);

    QPixmap backImg (":/back.png"), nextImg (":/next.png");
    QIcon backIcon (backImg), nextIcon (nextImg);
    ui->backButton->setIcon(backIcon);
    ui->nextButton->setIcon(nextIcon);
    ui->backButton->setDisabled(1);
    ui->nextButton->setDisabled(1);
    ui->backButton->setIconSize(QSize(40,40));
    ui->nextButton->setIconSize(QSize(40,40));

    connect (ui->backButton,SIGNAL(clicked()),this,SLOT(onBackButtonClicked()));
    connect (ui->nextButton,SIGNAL(clicked()),this,SLOT(onNextButtonClicked()));
    backNextLayout = new QHBoxLayout;
    backNextLayout->addWidget(ui->backButton);
    backNextLayout->addWidget(ui->nextButton);
    backNextLayout->addSpacerItem(new QSpacerItem(200000,0,QSizePolicy::Maximum));
    //actions.push_back(0);
    it = actions.begin();

    mainLayout->addLayout(headerLayout);
    mainLayout->addLayout(backNextLayout);
    //TODO: Code below must be replaced

    //sol->show();
    showedData = new QWidget *[listEls];
    for (int i=0; i<listEls;++i)
        showedData[i] = nullptr;
    setLayout(mainLayout);

    connect (ui->menuOptions,SIGNAL(currentIndexChanged(int)),this,SLOT(selector(int)));
    connect (ui->logOffButton,SIGNAL(clicked()),this,SLOT(close()));

    selector(0);
}

void Viewer::createGotoTask()
{
    if (!showedData[1]) {
        showedData[1] = new GoToTask(this);
        connect (showedData[1],SIGNAL(forSubmission (QString)),this,SLOT(fromGoToTask(QString)));
        connect (this,SIGNAL(gotoIt(QString)),showedData[1],SLOT(setCurTask(const QString &)));
        connect (showedData[1],SIGNAL(toMySubmissions(QString)),this,SLOT(fromGotoTaskToMySubmissions (QString)));
        connect (this,SIGNAL(tranferTaskNameRequest(qint64,QString)),showedData[1],SLOT(obtainTaskName(qint64,QString)),
                Qt::DirectConnection);
    }
}

void Viewer::createSendSolution()
{
    if (!showedData[0]) {
        showedData[0] = new SendSolution(currentPass,this,currentTask);
        connect (this,SIGNAL(taskChanger(QString)),showedData[0],SLOT(reSetTask(const QString &)));
        connect (showedData[0],SIGNAL(successfullySent (const qint64 &, const QString &, const QString &, bool)),
                this,SLOT(fromSuccessfullSubmission(qint64,QString,QString,bool)));
    }
}

void Viewer::createTaskList()
{
    if (!showedData[2])
        showedData[2] = new TaskList (this);
}

void Viewer::createMySubmissions()
{
    if (!showedData[3]) {
        showedData[3] = new MySubmissions (this);
        connect (showedData[3],SIGNAL(taskNameRequest (const qint64 &, const QString &)),
                this,SLOT(taskNameRequestToGotoTask(const qint64 &, const QString &)));
        connect (this,SIGNAL(toMySubmission(const qint64&, const QString &,const QString&,bool)),
                 showedData[3],SLOT(processSubmission(const qint64 &,const QString &, const QString &, bool)));
        connect (this,SIGNAL(mySubmissionFiltered(QString)),showedData[3],SLOT(displayByProblem(QString)));
        //showedData[3]->setVisible(1);
        showedData[0]->setFocus();
        mainLayout->addWidget(showedData[3]);
        showedData[3]->setVisible(1);
        QMetaObject::invokeMethod(showedData[3],"emitter");
        qDebug () << "Creating MySubmissions\n";
    }
}

Viewer::~Viewer()
{
    delete showedData;
    delete ui;
}

void Viewer::selector(int option)
{
    //qDebug() << "PO\n";
    if (prevInd!=-1) {
        mainLayout->removeWidget(showedData[prevInd]);
        showedData [prevInd]->setVisible(0);
    }
    switch (option) {
    case 0:
        createSendSolution();
        //mainLayout->addWidget(showedData[0]);
        break;
    case 1:
        createGotoTask();
        //mainLayout->addWidget(showedData[1]);
        break;
    case 2:
        createTaskList();
        break;
    case 3:
        createMySubmissions();
        qDebug () << "Boo\n";
        break;
    default:
        qFatal("Check the combobox!\n");
        break;
    }
    showedData[0]->setFocus();
    mainLayout->addWidget(showedData[option]);
    showedData[option]->setVisible(1);
    prevInd = option;

    if (!cameFromButton)
    {
        it = actions.erase(actions.begin(),it);
        actions.push_front(option);
        it = actions.begin();
    }
    ui->backButton->setDisabled(actions.length()<2 || it==actions.end()-1);
    ui->nextButton->setDisabled(actions.length()<2 || it==actions.begin());
    cameFromButton = false;
    qDebug () << "Boo\n";
}

void Viewer::fromGoToTask(const QString& x)
{
    currentTask = x;
    ui->menuOptions->setCurrentIndex(0);
    emit taskChanger(x);
}

void Viewer::fromTaskList(const QString &x)
{
    ui->menuOptions->setCurrentIndex(1);
    emit gotoIt(x);
}

void Viewer::fromGotoTaskToMySubmissions(const QString &s)
{
    ui->menuOptions->setCurrentIndex(3);
    emit mySubmissionFiltered(s);
}

void Viewer::fromSuccessfullSubmission(const qint64 &id,
                                       const QString &taskNum,
                                       const QString &dateTime,
                                       bool firstTestOnly)
{
    ui->menuOptions->setCurrentIndex(3);
    emit toMySubmission(id, taskNum, dateTime, firstTestOnly);
}

void Viewer::onBackButtonClicked()
{
    cameFromButton = true;
    ++it;
    ui->menuOptions->setCurrentIndex(*it);
}

void Viewer::onNextButtonClicked()
{
    cameFromButton = true;
    --it;
    ui->menuOptions->setCurrentIndex(*it);
}

void Viewer::taskNameRequestToGotoTask(const qint64 &id, const QString &taskName)
{

    qDebug() << "Entered Viewer::taskNameRequestToGotoTask with args " << id << " " << taskName << "\n";
    //showedData[3]->blockSignals(1);
    createGotoTask();
    connect (showedData[1],SIGNAL(replyTaskName (const qint64 &,const QString&)),
            showedData[3],SLOT(getTaskName(const qint64 &, const QString &)),Qt::UniqueConnection);
    emit tranferTaskNameRequest(id, taskName);
    //showedData[3]->blockSignals(0);
}
