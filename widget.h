#ifndef WIDGET_H
#define WIDGET_H

#include "global.h"

class Widget : public QWidget
{
    Q_OBJECT
    
public:
    Widget(QWidget *parent = 0);

    ~Widget();
public slots:
    void replyFinished(QNetworkReply *);
    void loadPage();
    void logIn ();
    void onLogInFinished (QNetworkReply *);
    void onFirstStepFinished (QNetworkReply *);
    void onSecondStepFinished (QNetworkReply *);
private:   
    QNetworkAccessManager *statusMonitor;
    QString str;
    QLabel *red, *loginLabel, *passLabel;
    QLabel *green;
    QLabel *yellow;
    QLineEdit *loginLine, *passLine;
    QPushButton *loginButton;
    QTimer *timer;
    //QString currentLogin;
    void setToWait ();
    void clean ();
    QTextCodec *codec=QTextCodec::codecForName("Windows-1251");
    bool loginSucceeded = false;
};

#endif // WIDGET_H
