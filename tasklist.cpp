#include "tasklist.h"
#include <iostream>

TaskList::TaskList(QWidget *parent) :
    QWidget(parent),
    par (parent)
{
    table = new QTableWidget (numOfTasks,3,this);
    //mapper = new QSignalMapper (this);
    taskNum = new QTableWidgetItem (tr("№"),0);
    taskName = new QTableWidgetItem (tr("Task Name"),0);
    ifAccepted = new QTableWidgetItem (tr("Status"),0);
    //gotoTask = new QTableWidgetItem (tr("Go to\ntask"),0);

    timeOutToRespond = new QTimer;
    connect (timeOutToRespond,SIGNAL(timeout()),this,SLOT(processError()));

    table->setHorizontalHeaderItem(0, taskNum);
    table->setHorizontalHeaderItem(1,taskName);
    table->setHorizontalHeaderItem(2,ifAccepted);
    table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    //table->horizontalHeader()->setStretchLastSection(1);
    table->verticalHeader()->setVisible(0);
    connect (table,SIGNAL(cellClicked(int,int)),this,SLOT(signalEmitter(int,int)));

    layout = new QVBoxLayout (this);
    layout->addWidget(table);

    progress = new QProgressBar (this);
    progress->setAlignment(Qt::AlignCenter);
    progress->setRange(0,100);
    progress->setGeometry(par->width()/2-100,par->height()/2-30,200,30);
    progress->setValue(0);
    progress->setVisible(0);

    connect (this,SIGNAL(forGotoTask(QString)),par,SLOT(fromTaskList(const QString &)));
    //layout->update();
    //qDebug () << width() << " " << height() << "\n";

    filterLayout = new QHBoxLayout;
    filterLabel = new QLabel ("Filter tasks by status:",this);
    selectFilter = new QComboBox (this);
    selectFilter->addItems({"None","Accepted","Failed","Yet not tried"});
    connect (selectFilter,SIGNAL(currentIndexChanged(int)),this,SLOT(onFilterChanged(int)));
    filterLayout->addWidget(filterLabel);
    filterLayout->addWidget(selectFilter);

    updateButton = new QPushButton ("Update tasklist", this);
    layout->addWidget(updateButton);
    layout->addLayout(filterLayout);
    connect(updateButton,SIGNAL(clicked()),this,SLOT(updateList()));

    popUpButton = new QPushButton ("Connection error occured.\n Press here to update",this);
    popUpButton->setGeometry(par->width()/2-100,par->height()/2-50,200,70);
    popUpButton->setVisible(0);
    connect(popUpButton,SIGNAL(clicked()),this,SLOT(updateList()));

    setLayout(layout);
    updateList();
}

TaskList::~TaskList()
{
    delete timeOutToRespond;
}

void TaskList::addToTable()
{
    QNetworkReply *repl = qobject_cast <QNetworkReply*> (sender());
    sender()->deleteLater();
    qDebug() << "Entered addToTable\n";
    if (repl->error() != QNetworkReply::NoError)
    {
        repl->deleteLater();
        processError();
        return;
    }
    timeOutToRespond->stop();
    QTextCodec *codec=QTextCodec::codecForName("Windows-1251");
    QByteArray dat = repl->readAll();
    repl->deleteLater();
    QString ss = codec->toUnicode(dat.data());
    ss.remove(0,ss.indexOf("<TD align=\'center\'")+1);
    int pos = ss.indexOf("</TABLE>");
    ss.remove(pos,ss.size()-pos+1);
    ss+="\n";
    while (!ss.isEmpty())
    {

        //qDebug () << ss << "\n";
        //qDebug () << ss.indexOf("\n") << "\n";
        //qDebug() << ss.mid(0,ss.indexOf("\n")) << "\n";
        //std::cin.get();
        pos = ss.indexOf("&id=")+4;
        //qDebug () << pos << "\n";
        QString curTask = ss.mid(pos,4);
        //qDebug () << curTask << "\n";
        pos = ss.indexOf(".gif")-3;
        //qDebug() << ss[pos-1] << ss[pos] << ss[pos+1] << "\n";
        int status;
        if (ss[pos]=='l') status = 0;
        else if (ss[pos]=='e') status = 1;
        else status = 2;

        int posBegName = ss.indexOf("&nbsp;")+6;
        int posEndName = ss.indexOf("</A><br>");


        table->setItem(curT,0,new QTableWidgetItem(curTask,0));
        table->setItem(curT,1,new QTableWidgetItem(ss.mid(posBegName,posEndName-posBegName),0));
        if (2 == status) table->setItem(curT,2,new QTableWidgetItem("Accepted",0));
        else if (1 == status) table->setItem(curT,2,new QTableWidgetItem("Incompleted",0));
        else table->setItem(curT,2,new QTableWidgetItem("Failed",0));
//        QPushButton *temp = new QPushButton;
//        temp->setMaximumWidth(30);
//        temp->setText(">>");
//        table->setCellWidget(curT,3,temp);
//        connect (temp,SIGNAL(clicked()),mapper,SLOT(map()));
        //qDebug() << ss.mid(0,ss.indexOf("\n")) << "\n";
        ss.remove(0,ss.indexOf("\n")+1);

        ++curT;
        progress->setValue((static_cast <double> (curT)/numOfTasks)*100);
//        if (curTask=="1049")
//        {
//            qDebug() << ss << "\n";
//            std::cin.get();
//        }
    }
    progress->hide();
    progress->setValue(0);
    table->setDisabled(0);
    updateButton->setDisabled(0);
        //table->setVisible(1);
        //table->resizeColumnsToContents();

        //disconnect(reply,SIGNAL(finished()),this,SLOT(addToTable()));
    selectFilter->setDisabled(0);

}

void TaskList::signalEmitter(int row, int col)
{
    //qDebug () << table->item(row,0)->text() << "\n";
    emit forGotoTask(table->item(row,0)->text());
    //qDebug () << "sh\n";
}

void TaskList::updateList()
{
    curT = 0;
    table->setDisabled(1);
    table->clearContents();

    updateButton->setDisabled(1);
    popUpButton->setVisible(0);

    progress->setValue(0);
    progress->show();
    timeOutToRespond->start(30000);
    QNetworkReply *rr = GlobalManager::netw->get(QNetworkRequest(QUrl("http://acm.lviv.ua/fusion/viewpage.php?page_id=21")));
    connect (rr,SIGNAL(finished()),this,SLOT(addToTable()));
    selectFilter->setDisabled(1);
}

void TaskList::processError()
{
    qDebug () << "Entered processError ()\n";
    timeOutToRespond->stop();
    table->clearContents();
    popUpButton->show();
    progress->hide();
    progress->setValue(0);
    //disconnect(reply, SIGNAL(finished()), this, SLOT(addToTable()));

}

void TaskList::onFilterChanged(int opt)
{

    for (int i=0 ;i<numOfTasks; ++i) {
        table->setRowHidden(i,false);
        switch (opt) {
        case 1:
            if (table->item(i,2)->text()!="Accepted")
                table->setRowHidden(i,true);
            break;
        case 2:
            if (table->item(i,2)->text()!="Failed")
                table->setRowHidden(i,true);
            break;
        case 3:
            if (table->item(i,2)->text()!="Incompleted")
                table->setRowHidden(i,true);
            break;
        default:
            break;
        }
    }
}


