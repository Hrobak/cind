#include "sendsolution.h"
#include "ui_sendsolution.h"

SendSolution::SendSolution(const QString &pass,QWidget *parent,const QString &tas) :
    QWidget(parent),
    password(pass),
    task (tas),
    ui(new Ui::SendSolution)
{
    ui->setupUi(this);
    ui->taskNumber->setText(tas);

    highlighter = new Highlighter;
    highlighter->setDocument(ui->textEdit->document());

    ui->radioC->setChecked(true);
    connect(ui->sendButton,SIGNAL(clicked()),this,SLOT(sendData()));
    connect(ui->fileChooser,SIGNAL(clicked()),this,SLOT(getFile()));
    connect(ui->taskNumber,SIGNAL(editingFinished()),this,SLOT(checkIfTaskExists()));
}

void SendSolution::reSetTask(const QString &ss)
{
    task = ss;
    ui->taskNumber->setText(ss);
}

void SendSolution::getFile()
{
    QString pathToFile = QFileDialog::getOpenFileName(this,tr("Open File"));
    QFile file (pathToFile);
    //qDebug () << pathToFile << "\n";
    if (!file.open(QIODevice::ReadOnly))
        qDebug () << "LAGA\n";
    ui->textEdit->setPlainText(file.readAll());
}

void SendSolution::checkIfTaskExists()
{
    ui->sendButton->setDisabled(1);
    QString task = ui->taskNumber->text();
    bool ok; int temp;
    if (task.length() != 4 || !(temp = task.toInt(&ok,10),ok) || (temp<1000) || (temp > 1395))
    {
        ui->taskNumber->setText("Wrong Task");
        taskNumberIsOK = false;
        ui->sendButton->setDisabled(0);
        return;
    }
    this->task = task;
    QUrl urlForCheck ("http://acm.lviv.ua/fusion/viewpage.php?page_id=9&id="+task.toUtf8());
    //qDebug () << urlForCheck << "\n";

    QNetworkRequest request (urlForCheck);
    //connect (SingleTon::netw,SIGNAL(finished(QNetworkReply*)),this,SLOT(checkReply(QNetworkReply*)));
    QNetworkReply *rr = GlobalManager::netw->get(request);
    connect (rr,SIGNAL(finished()),this,SLOT(checkReply()));

}

void SendSolution::checkReply()
{
    QNetworkReply *repl = qobject_cast <QNetworkReply *> (sender());
    sender()->deleteLater();
    int response = GlobalManager::checkResponse(repl,"Задачу в архіві не знайдено");
    if (2 == response)
    {
        ui->taskNumber->setText("Connection Error");
        taskNumberIsOK = false;
    }
    else if (1 == response)
    {
        taskNumberIsOK  =false;
        ui->taskNumber->setText("Wrong Task");
    }
    else
    {
        taskNumberIsOK  = true;
    }
    repl->deleteLater();
    //disconnect(SingleTon::netw,SIGNAL(finished(QNetworkReply*)),this,SLOT(checkReply(QNetworkReply*)));
    ui->sendButton->setDisabled(0);
}


SendSolution::~SendSolution()
{

    delete ui;
    delete highlighter;
}

void SendSolution::sendData()
{
    if (!taskNumberIsOK)
        return;

    firstTestOnly = ui->firstTestOnly->isChecked();
    task = ui->taskNumber->text();

    QByteArray postData;
    QUrl sendUrl ("http://acm.lviv.ua/fusion/acm/test.php?");

    postData += "userlgn=" + QUrl::toPercentEncoding(GlobalManager::currentLogin.toUtf8());
    postData += "&userpassw=" + QUrl::toPercentEncoding(password.toUtf8());
    postData += "&id=" + QUrl::toPercentEncoding(ui->taskNumber->text().toUtf8());
    postData += ui->radioPascal->isChecked()?"&lang=3":"&lang=2";
    postData += "&source=" + QUrl::toPercentEncoding(ui->textEdit->toPlainText().toUtf8());
    if (ui->firstTestOnly->isChecked())
        postData += "&firstonly=on";
    //qDebug() << QString (postData) << "\n\n";
    QNetworkRequest request (sendUrl);
    request.setRawHeader("Accept-Encoding","identity");
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
    QNetworkReply *rr = GlobalManager::netw->post(request,postData);
    connect (rr,SIGNAL(finished()),this,SLOT(processPostReply()));

//    QNetworkReply *repl = GlobalManager::netw->get(
//                QNetworkRequest(QUrl("http://acm.lviv.ua/fusion/viewpage.php?page_id=1&fromID=159293")));
//    connect (repl,SIGNAL(finished()),SLOT(processGetReply()));

    ui->sendButton->setDisabled(1);
    //qDebug() << password << "\n";



}

void SendSolution::processPostReply ()
{
    QNetworkReply *repl = qobject_cast <QNetworkReply *> (sender());
    sender()->deleteLater();
    if (repl->error() == QNetworkReply::NoError)
    {
        QNetworkReply *repl = GlobalManager::netw->get(
                    QNetworkRequest(QUrl("http://acm.lviv.ua/fusion/viewpage.php?page_id=1&filter_probid="+task)));
        connect (repl,SIGNAL(finished()),SLOT(processGetReply()));
    }
    else
    {
        QMessageBox box;
        box.setText("Connection has been lost.\n Try to submit later");
        box.exec();
        qDebug () << repl->errorString() << "\n";
    }
    ui->sendButton->setDisabled(0);

    repl->deleteLater();
    //disconnect (SingleTon::netw,SIGNAL(finished(QNetworkReply*)),this,SLOT(debugO(QNetworkReply*)));

}

void SendSolution::processGetReply()
{
    QNetworkReply *repl = qobject_cast <QNetworkReply *> (sender());
    sender ()->deleteLater();
    if (repl->error() != QNetworkReply::NoError) {
        QMessageBox box;
        box.setText("Connection has been lost.\n Please check this submission manually");
        box.exec();
    }
    else {
        QTextCodec *codec=QTextCodec::codecForName("Windows-1251");
        QByteArray data = repl->readAll();
        QString ss = codec->toUnicode(data.data());
        //qDebug () << ss << "\n\n";
        GlobalManager::cutResponse(ss);
        QTextStream stream (&ss);
        stream.readLine();
        QString posId, posDate, posTime, posUserName;
        while (!stream.atEnd()) {
            posId = stream.readLine();
            posDate = stream.readLine();
            posTime = stream.readLine();
            posUserName = stream.readLine();
            if (posUserName == GlobalManager::currentLogin) {
                qDebug() << posDate + "\n" + posTime << "\n";
                emit successfullySent(posId.toLongLong(),
                                      task,
                                      posDate + "\n" + posTime,
                                      firstTestOnly);
                return;
            }
            for (int i=0; i<6 && !stream.atEnd(); ++i)
                stream.readLine();
        }
    }
    ui->sendButton->setDisabled(0);

}
