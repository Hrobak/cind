#include "widget.h"
#include "global.h"
#include "viewer.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    if (!GlobalManager::netw)
        GlobalManager::netw = new QNetworkAccessManager;
    statusMonitor = new QNetworkAccessManager (this);
    //connect(netw,SIGNAL(finished(QNetworkReply*)),this,SLOT(replyFinished(QNetworkReply*)));
    connect(statusMonitor,SIGNAL(finished(QNetworkReply*)),this,SLOT(replyFinished(QNetworkReply*)));
    QHBoxLayout *mai = new QHBoxLayout (this);
    QVBoxLayout *lay = new QVBoxLayout;
    red = new QLabel(this);
    green = new QLabel (this);
    yellow = new QLabel (this);
    red->setMinimumWidth(200);
    yellow->setMinimumWidth(200);
    green->setMinimumWidth(200);
    loginLine = new QLineEdit (this);
    passLine = new QLineEdit (this);
    passLine->setEchoMode(QLineEdit::Password);

    loginLabel = new QLabel (this);
    loginLabel->setText("Login");
    passLabel = new QLabel (this);

    loginLabel->setBuddy(passLabel);
    passLabel->setText("Password");


    loginButton = new QPushButton (this);
    passLabel->setBuddy(loginButton);
    loginButton->setText("Enter");
    connect (loginButton,SIGNAL(clicked()),this,SLOT(logIn()));

    setToWait();
    timer = new QTimer (this);
    lay->addWidget(red);
    lay->addWidget(yellow);
    lay->addWidget(green);
    clean();
    connect(timer,SIGNAL(timeout()),this,SLOT(loadPage()));
    timer->start(30000); //Timeout for page refresh
    loadPage();

    QVBoxLayout *lay2 = new QVBoxLayout;
    lay2->addWidget(loginLabel);
    lay2->addWidget(loginLine);
    lay2->addWidget(passLabel);
    lay2->addWidget(passLine);
    lay2->addWidget(loginButton);
    mai->addLayout(lay);
    mai->addLayout(lay2);
    setLayout(mai);

    setMaximumHeight(150);
    setMaximumWidth(400);

}

void Widget::loadPage()
{
    setToWait();
    statusMonitor->get(QNetworkRequest(QUrl("http://acm.lviv.ua/fusion/viewpage.php?page_id=1")));
}

void Widget::logIn()
{
    qDebug () << "Hello there\n";
#ifdef QT_NO_DEBUG
    SingleTon::currentLogin = loginLine->text();
    QString login = loginLine->text();
#else
    GlobalManager::currentLogin = "Esperanto";
    QString login = "Esperanto";
#endif
    QUrl urlDoLogin ("http://acm.lviv.ua/fusion/login.php");

#ifdef QT_NO_DEBUG
    QString pass = passLine->text();
#else
    QString pass = "password";
#endif
    qDebug () << pass << "\n";
    QByteArray postData;
    postData += "user_name=" + QUrl::toPercentEncoding(login.toUtf8());

    postData += "&user_pass=" + QUrl::toPercentEncoding(pass.toUtf8());
    postData += "&remember_me=y";
    postData += "&login=Login";
    //qDebug() << postData << "\n";
    //exit(0);
    QNetworkRequest request(urlDoLogin);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
    //request.setHeader(QNetworkRequest::UserAgentHeader,"Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22");
    request.setRawHeader("Accept-Encoding","identity"); // Отменяю сжатие что бы, можно было нормально просматривать контент снифером

    connect(GlobalManager::netw, SIGNAL(finished(QNetworkReply *)),
             this, SLOT(onFirstStepFinished(QNetworkReply*))
           );
    GlobalManager::netw->post(request,postData);

    //disconnect(netw);

}

//////////////////////////////////////////////////
/// \brief Widget::onLogInFinished
/// \param repl
/// TEMPORARY FUNCTION FOR DEBUG ONLY
//////////////////////////////////////////////////

void Widget::onLogInFinished(QNetworkReply *repl)
{
    //TODO: Get rid of this function
    if (repl->error() == QNetworkReply::NoError)
    {
        QByteArray resp = repl->readAll();
        QString ss = codec->toUnicode(resp.data());
        //qDebug () << ss << "\n";
    }
        //qDebug() << "SUCCESS\n";
    else
        qDebug() << repl->errorString() << "\n";
}

void Widget::onFirstStepFinished(QNetworkReply *)
{

    disconnect(GlobalManager::netw,SIGNAL(finished(QNetworkReply*)),this,SLOT(onFirstStepFinished(QNetworkReply*)));
    connect(GlobalManager::netw,SIGNAL(finished(QNetworkReply*)),this,SLOT(onSecondStepFinished(QNetworkReply*)));
    GlobalManager::netw->get(QNetworkRequest(QUrl("http://acm.lviv.ua/fusion/setuser.php?user="+GlobalManager::currentLogin)));
}
////////////////////////////////////////////
/// \brief Widget::onSecondStepFinished
/// Obtaining result of login attempt
////////////////////////////////////////////

void Widget::onSecondStepFinished(QNetworkReply *repl)
{
    QByteArray dat = repl->readAll();
    repl->deleteLater();
    QString ss = codec->toUnicode(dat.data());
    //qDebug () << ss << "\n";
    //qDebug () << currentLogin << "\n";
    if (!ss.contains("Зайти як "+GlobalManager::currentLogin))
    {

        loginLine->clear();
        passLine->clear();
        loginLine->setText("Invalid login or password");
        //TODO: We should create a derived class for loginLine and reimplement focusEvent
    }
    else
    {
        loginLine->setDisabled(true);
        passLine->setDisabled(true);
        loginButton->setDisabled(true);
        loginSucceeded = true;
    }
    disconnect(GlobalManager::netw,SIGNAL(finished(QNetworkReply*)),this,SLOT(onSecondStepFinished(QNetworkReply*)));
    //connect(SingleTon::netw,SIGNAL(finished(QNetworkReply*)),this,SLOT(onLogInFinished(QNetworkReply*)));
    GlobalManager::netw->get(QNetworkRequest(QUrl("http://acm.lviv.ua/fusion/news.php")));
    if (loginSucceeded)
    {
#ifndef QT_DEBUG
        Viewer *view = new Viewer (passLine->text());
#else
        Viewer *view = new Viewer ("password");
#endif
        //view->setModal (1);
        //view->show();
        view->exec();
        delete view;
        loginLine->setDisabled(false);
        passLine->setDisabled(false);
        loginButton->setDisabled(false);
        GlobalManager::netw->clearAccessCache();
        GlobalManager::netw->setCookieJar(new QNetworkCookieJar());
        passLine->clear();
        loginSucceeded = false;
    }
}

void Widget::setToWait()
{
    clean ();
    yellow->setStyleSheet("background-color: yellow;");
    yellow->setText("UPDATING STATUS");
}

void Widget::clean()
{
    red->clear();
    yellow->clear();
    green->clear();
    red->setStyleSheet("background-color: grey;"
                       "qproperty-alignment: AlignCenter;");
    green->setStyleSheet("background-color: grey;"
                         "qproperty-alignment: AlignCenter;");
    yellow->setStyleSheet("background-color: grey;"
                          "qproperty-alignment: AlignCenter;");

}

Widget::~Widget()
{
    
}

///////////////////////////////////////////////////
/// \brief Widget::replyFinished
/// \param repl
/// Displaying system status
///////////////////////////////////////////////////

void Widget::replyFinished(QNetworkReply* repl)
{
    int response = GlobalManager::checkResponse(repl,"Поставлено в чергу");

    if (2 == response)
    {
        clean();
        red->setStyleSheet("background-color: red;");
        yellow->setStyleSheet("background-color: yellow;");
        yellow->setText("NETWORK ERROR");
    }
    else if (1 == response)
    {
        clean ();
        red->setStyleSheet("background-color: red;");
        red->setText("JUDGING IN QUEUE");

    }
    else
    {
        clean ();
        green->setStyleSheet("background-color: green;");
        green->setText("JUDGE SYSTEM IS OK");
    }
}
