#ifndef VIEWER_H
#define VIEWER_H

#include "global.h"

namespace Ui {
class Viewer;
}

class Viewer : public QDialog
{
    Q_OBJECT

public:
    Viewer(const QString &pass, QDialog *parent = 0);
    void createGotoTask ();
    void createSendSolution ();
    void createTaskList ();
    void createMySubmissions ();
    ~Viewer();
signals:
    void taskChanger (const QString &);
    void gotoIt (const QString &);
    void toMySubmission (const qint64 &,const QString &, const QString &, bool);
    void mySubmissionFiltered (const QString &);
    void tranferTaskNameRequest (const qint64 &, const QString &);
public slots:
    void selector (int);
    void fromGoToTask(const QString &);
    void fromTaskList (const QString &);
    void fromGotoTaskToMySubmissions (const QString &);
    void fromSuccessfullSubmission (const qint64 &, const QString &, const QString &, bool);
    void onBackButtonClicked ();
    void onNextButtonClicked ();
    void taskNameRequestToGotoTask (const qint64 &, const QString &);
private:
    const static int listEls = 4;
    QList <int> actions;
    QList <int>::Iterator it;
    bool cameFromButton = false;
    int prevInd = -1;
    QString currentTask = "1000";
    Ui::Viewer *ui;
    QString currentPass;
    QVBoxLayout *mainLayout;
    QHBoxLayout *headerLayout, *backNextLayout;
    QWidget **showedData;
};

#endif // VIEWER_H
