#ifndef SENDSOLUTION_H
#define SENDSOLUTION_H

#include "global.h"
#include "highlighter.h"

namespace Ui {
class SendSolution;
}

class SendSolution : public QWidget
{
    Q_OBJECT
    
public:
    SendSolution(const QString &pass, QWidget *parent = 0, const QString &tas = "1000");

    ~SendSolution();
    
private:
    QString password, task;
    Ui::SendSolution *ui;
    bool taskNumberIsOK = true, firstTestOnly = false;
    Highlighter *highlighter;

public slots:
    void sendData ();
    void reSetTask (const QString &);
    void processPostReply ();
    void processGetReply ();
    void getFile ();
    void checkIfTaskExists ();
    void checkReply();

signals:
    void successfullySent (const qint64 &, const QString &, const QString &, bool);
};

#endif // SENDSOLUTION_H
