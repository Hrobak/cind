#include "mysubmissions.h"
#include "solutionviewer.h"

MySubmissions::MySubmissions(QWidget *parent) :
    QWidget(parent)
{
    userSubmissionFile = new QFile (GlobalManager::currentLogin);
    if (!userSubmissionFile->open(QIODevice::ReadWrite))
    {
        qFatal("Unable to create file. Please be sure you have enough space\nand proper access to the folder");
    }

    QTextStream stream(userSubmissionFile);

    table = new QTableWidget (0,4,this);
    table->setHorizontalHeaderItem(0,new QTableWidgetItem("Task #"));
    table->setHorizontalHeaderItem(1,new QTableWidgetItem("Task name"));
    table->setHorizontalHeaderItem(2,new QTableWidgetItem("Result"));
    table->setHorizontalHeaderItem(3,new QTableWidgetItem("Date"));
    table->verticalHeader()->setVisible(0);
    table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    table->verticalHeader()->setDefaultSectionSize(50);
    table->setEditTriggers(QAbstractItemView::NoEditTriggers);
    connect (table,SIGNAL(cellClicked(int,int)),this,SLOT(onTableCellClicked(int,int)));

    filterByProblemLabel = new QLabel ("Filter by problem number",this);
    filterTaskNumber = new QLineEdit (this);
    filterButton = new QPushButton ("Filter",this);
    resetFilterButton = new QPushButton ("Reset Filter",this);
    filterByProblemLayout = new QHBoxLayout;
    filterByProblemLayout->addWidget(filterByProblemLabel);
    filterByProblemLayout->addWidget(filterTaskNumber);
    filterByProblemLayout->addWidget(filterButton);
    filterByProblemLayout->addWidget(resetFilterButton);
    connect (filterButton,SIGNAL(clicked()),this,SLOT(getFilterTaskNumber()));
    connect (resetFilterButton,SIGNAL(clicked()),this,SLOT(displayByProblem()));

    layout = new QVBoxLayout (this);
    layout->addWidget(table);
    layout->addLayout(filterByProblemLayout);

    QString taskNum, taskName, taskRes, taskDate; qint16 isFTest;
    qint64 submId;
    while (!stream.atEnd())
    {
        stream >> submId >> taskNum >> taskName >> taskRes >> taskDate >> isFTest;
        qDebug () << submId << " " << taskNum << " " << taskName << " " << taskRes << " " << taskDate << " " << isFTest << "\n";
        if (stream.atEnd()) break;
        taskName.replace("_"," ");
        taskRes.replace("_"," ");
        taskDate.replace ("_","\n");
        submissionsData [submId] = Submission (taskNum, taskName, taskRes, taskDate,isFTest);
        if (taskRes == "Waiting")
            inQueue.push_back(submId);
    }
    foreach (qint64 iter, submissionsData.keys()) {
        Submission it = submissionsData.value(iter);
        table->insertRow(0);
        table->setItem(0,0,new QTableWidgetItem (it.num));
        table->setItem(0,1,new QTableWidgetItem (it.name));
        table->setItem(0,2,new QTableWidgetItem (it.resu));
        table->setItem(0,3,new QTableWidgetItem (it.date));
        if (it.isForFirstTestOnly)
            for (int i=0;i<4;++i)
                table->item(0,i)->setBackgroundColor(QColor("lightgrey"));
        rowOfSubmission[iter] = table->rowCount();
        if (it.name == "Updating")
//            QMetaObject::invokeMethod(this,"taskNameRequest",Qt::QueuedConnection,
//                                      Q_ARG(qint64,iter),Q_ARG(QString,it.num));
            updatingQueue.append(qMakePair(iter,it.num));
    }
    userSubmissionFile->close();

    inQueueUpdater = new QTimer (this);
    connect (inQueueUpdater,SIGNAL(timeout()),this,SLOT(checkQueue()));
    inQueueUpdater->start(1000);

    setLayout(layout);
}

void MySubmissions::emitter()
{
    QMutableListIterator <QPair<qint64,QString>> it (updatingQueue);
    while (it.hasNext()) {
        it.next();
        emit taskNameRequest(it.value().first,it.value().second);
        it.remove();
    }
}

MySubmissions::~MySubmissions()
{
    userSubmissionFile->open(QIODevice::WriteOnly);
    QTextStream stream (userSubmissionFile);
    QString temp;
    foreach (qint64 it, submissionsData.keys()) {
        temp = submissionsData.value(it).name;
        stream << it << " " << submissionsData.value(it).num << " ";
        temp.replace(" ","_");
        stream << temp << " ";
        temp = submissionsData.value(it).resu;
        temp.replace(" ","_");
        stream << temp << " ";
        temp = submissionsData.value(it).date;
        temp.replace ("\n","_");
        stream << temp << " ";
        stream << (submissionsData.value(it).isForFirstTestOnly) << "\n";
    }
    userSubmissionFile->close();
}

void MySubmissions::getFilterTaskNumber()
{
    displayByProblem(filterTaskNumber->text());
}

void MySubmissions::processSubmission(const qint64 &id,
                                      const QString &taskNum,
                                      const QString &dateTime, bool firstTestOnly)
{
    table->insertRow(0);
    table->setItem(0,0,new QTableWidgetItem (taskNum));
    table->setItem(0,1,new QTableWidgetItem ("Updating"));
    table->setItem(0,2,new QTableWidgetItem ("Waiting"));
    table->setItem(0,3,new QTableWidgetItem (dateTime));
    if (firstTestOnly)
        for (int i=0; i<4; ++i)
            table->item(0,i)->setBackgroundColor(QColor("lightgrey"));

    inQueue.push_back(id);
    submissionsData [id] = Submission (taskNum, "Updating", "Waiting", dateTime, firstTestOnly?1:0);
    rowOfSubmission [id] = table->rowCount();
    emit taskNameRequest(id,taskNum);
}

void MySubmissions::checkQueue()
{
    inQueueUpdater->stop();
    QMutableListIterator <qint64> it (inQueue);
    QEventLoop loop;
    QTextCodec *codec=QTextCodec::codecForName("Windows-1251");
    qDebug() << "Hello Moto\n";
    while (it.hasNext())
    {
        QNetworkReply *repl = GlobalManager::netw->get(QNetworkRequest(QUrl("http://acm.lviv.ua/fusion/viewpage.php?page_id=1&fromID="+QString::number(it.next()))));
        connect (repl,SIGNAL(finished()),&loop,SLOT(quit()));
        loop.exec();
        if (repl->error() == QNetworkReply::NoError) {
            QByteArray dat = repl->readAll();
            repl->deleteLater();
            QString ss = codec->toUnicode(dat.data());
            QTextStream stream (&ss);
            GlobalManager::cutResponse(ss);
            for (int i=0; i<7; ++i)
                stream.readLine();
            QString res = stream.readLine();
            qDebug () << res << "\n";
            if (res.contains("Поставлено в чергу"))
                continue;
            QString text;
            if (res.contains("Помилка компіляції"))
                text = "Compilation Error";
            else if (res.contains("Помилка часу"))
                text = "Runtime Error";
            else if (res.contains("Ліміт часу"))
                text = "Time Limit Exceeded";
            else if (res.contains("Ліміт пам"))
                text = "Memory Limit Exceeded";
            else if (res.contains("Неправильна відповідь"))
                text = "Wrong Answer";
            else if (res.contains("Зараховано"))
                text = "Accepted";
            else
                text = "Ignored";
            table->item(table->rowCount()-rowOfSubmission[it.value()],2)->setText(text);
            submissionsData [it.value()].resu = text;
            it.remove();
            continue;
        }
        repl->deleteLater();


    }
    inQueueUpdater->start(20000);
}

void MySubmissions::onTableCellClicked(int row, int col)
{
    SolutionViewer *view;
    qint64 id;
    foreach (qint64 it, rowOfSubmission.keys()) {
        if (rowOfSubmission.value(it) == table->rowCount()-row)
        {
            id = it;
            break;
        }
    }
    QNetworkReply *repl = GlobalManager::netw->get(QNetworkRequest(QUrl("http://acm.lviv.ua/fusion/viewpage.php?page_id=12&SubmitID="+QString::number(id))));
    QEventLoop loop;
    connect (repl,SIGNAL(finished()),&loop,SLOT(quit()));
    loop.exec();
    if (repl->error() == QNetworkReply::NoError)
    {
        QByteArray dat = repl->readAll();
        QString ss = GlobalManager::codec->toUnicode(dat.data());
        int beg = ss.indexOf("<PRE>")+5;
        int end = ss.indexOf("</PRE>");
        qDebug () << beg << " " << end;
        ss= (ss.mid(beg,end-beg));
        ss.replace("&lt;","<");
        ss.replace("&gt;",">");
        ss.replace("&amp;","&");
        ss.replace("&quot;","\"");
        ss.replace("&#039;","\'");
        view = new SolutionViewer (ss);
    }
    else
    {
        view = new SolutionViewer ("Connection Error");
    }
    repl->deleteLater();

    view->exec();
    view->setFocus();
}

void MySubmissions::displayByProblem(const QString &task)
{
    filterTaskNumber->setText(task);
    for (int i=0; i<table->rowCount();++i)
    {
        table->setRowHidden(i,false);
        if (!table->item(i,0)->text().contains(task))
            table->setRowHidden(i,true);
    }
}

void MySubmissions::displayByProblem()
{
    filterTaskNumber->clear();
    displayByProblem("");
}

void MySubmissions::getTaskName(const qint64 &id, const QString &taskName)
{
    qDebug () << id << " " << taskName << "\n";
    if (taskName != "ERR") {
        table->item(table->rowCount()-rowOfSubmission.value(id),1)->setText(taskName);
        submissionsData [id].name = taskName;
    }
    else
        emit taskNameRequest(id, table->item(table->rowCount()-rowOfSubmission.value(id),0)->text());
}
