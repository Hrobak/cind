#ifndef GOTOTASK_H
#define GOTOTASK_H

#include <QWidget>
#include "global.h"

namespace Ui {
class GoToTask;
}

class GoToTask : public QWidget
{
    Q_OBJECT
    
public:
    explicit GoToTask(QWidget *parent = 0, const QString &tas = "1000");
    ~GoToTask();
signals:
    void forSubmission (QString);
    void toMySubmissions (QString);
    void replyTaskName (const qint64 &,const QString&);
private:
    Ui::GoToTask *ui;
    QWidget *par;
    bool taskRequested = false;
    qint64 tempId;
    QString task;
public slots:
    void checkIfTaskExists ();
    void sendIt ();
    void setCurTask (const QString &);
    void moveToMySubmissions();
    void obtainTaskName (const qint64 &,const QString &);
};

#endif // GOTOTASK_H
