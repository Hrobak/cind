#include "gototask.h"
#include "ui_gototask.h"
#include <QThread>
#include <iostream>

GoToTask::GoToTask(QWidget *parent, const QString &tas) :
    QWidget(parent),
    par (parent),
    ui(new Ui::GoToTask)
{
    ui->setupUi(this);
    ui->taskNumber->setText(tas);


    connect (ui->toTaskButton,SIGNAL(clicked()),this,SLOT(checkIfTaskExists()));
    connect (ui->submitTask,SIGNAL(clicked()),this,SLOT(sendIt()));
    connect (ui->mySubmissionsButton,SIGNAL(clicked()),this,SLOT(moveToMySubmissions()));


}

GoToTask::~GoToTask()
{
    delete ui;
}

void GoToTask::checkIfTaskExists()
{
    QEventLoop loop;
    if (!taskRequested)
        task = ui->taskNumber->text();
    QNetworkReply *repl = GlobalManager::netw->get(QNetworkRequest(QUrl("http://acm.lviv.ua/fusion/viewpage.php?page_id=9&id="+task)));
    connect (repl, SIGNAL(finished()),&loop,SLOT(quit()));
    loop.exec();
    //qDebug () << "Entered GoToTask::checkIfTaskExists\n";
    if (repl->error() != QNetworkReply::NoError) {
        if (!taskRequested) {
            ui->taskNumber->setText("Connection Error");
            ui->submitTask->setDisabled(1);
            ui->taskText->clear();
        }
        else {
            emit replyTaskName(tempId, "ERR");
            taskRequested = false;
        }
    }
    else {

        QTextCodec *codec=QTextCodec::codecForName("Windows-1251");
        QByteArray dat = repl->readAll();
        QString ss = codec->toUnicode(dat.data());
        if (ss.contains("Задачу в архіві не знайдено")) {
            ui->taskNumber->setText("Wrong Task");
            ui->submitTask->setDisabled(1);
            ui->mySubmissionsButton->setDisabled(1);
            ui->taskText->clear();
        }
        else {
            int pos = ss.indexOf("<P align=center>");
            ss.remove(0,pos);
            pos = ss.indexOf("<a href=\"viewpage.php");
            ss.remove(pos,ss.size()-pos+1);
            ss.replace("<=","&le;");
            ss.replace(">=","&ge;");
            qDebug () << ss << "\n";
            if (!taskRequested) {
                ui->taskText->setText(ss);
                ui->submitTask->setDisabled(0);
                ui->mySubmissionsButton->setDisabled(0);
            }
            else {
                qDebug () << "SIGNAL EMITTED" << "\n";
                int p = ss.indexOf("<B>");
                ss.remove(0,p+10);
                emit replyTaskName(tempId, ss.mid(0,ss.indexOf("</B>")));
                taskRequested = false;
            }
        }
    }
    repl->deleteLater();
}


void GoToTask::sendIt()
{
    emit forSubmission(ui->taskNumber->text());
}

void GoToTask::setCurTask(const QString &x)
{
    ui->taskNumber->setText(x);
    emit ui->toTaskButton->clicked();
    //qDebug () << "sh\n";
}

void GoToTask::moveToMySubmissions()
{
    emit toMySubmissions(ui->taskNumber->text());
}

void GoToTask::obtainTaskName(const qint64 &id, const QString &taskNum)
{

    qDebug () << "Entered obtainTaskName with args " << id << " " << taskNum << "\n";
    taskRequested = true;
    tempId = id;
    task = taskNum;
    checkIfTaskExists();
}
