#ifndef TASKLIST_H
#define TASKLIST_H

#include <global.h>

class TaskList : public QWidget
{
    Q_OBJECT

    static const unsigned sets = 8;
    static const unsigned numOfTasks = sets*50-63;
    //QSignalMapper *mapper;
    QTableWidgetItem *taskNum, *taskName, *ifAccepted;//, *gotoTask;
    //QTableWidgetItem **listOfTasks;
    QTableWidget *table;
    int currentSet =0, curT = 0;
    QVBoxLayout *layout;
    QWidget *par;
    QProgressBar *progress;
    QPushButton *updateButton, *popUpButton;
    QTimer *timeOutToRespond;
    QNetworkReply *reply;
    QComboBox *selectFilter;
    QLabel *filterLabel;
    QHBoxLayout *filterLayout;

public:
    explicit TaskList(QWidget *parent = 0);
    ~TaskList ();
signals:
    void forGotoTask (const QString &);
public slots:
    void addToTable();
    void signalEmitter (int row, int col);

private slots:
    void updateList ();
    void processError ();
    void onFilterChanged (int);
};

#endif // TASKLIST_H
