#include "solutionviewer.h"

SolutionViewer::SolutionViewer(const QString &text, QWidget *parent) :
    QDialog(parent)
{
    edit = new QPlainTextEdit (this);
    edit->setPlainText(text);
    highlighter = new Highlighter;
    highlighter->setDocument(edit->document());
    layout = new QVBoxLayout (this);
    layout->addWidget(edit);
    setLayout(layout);
    setWindowTitle("View solution");
    //setWindowFlags(Qt::WindowStaysOnTopHint);
}

SolutionViewer::~SolutionViewer()
{
    delete highlighter;
}
