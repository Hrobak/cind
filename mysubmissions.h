#ifndef MYSUBMISSIONS_H
#define MYSUBMISSIONS_H

#include <QWidget>
#include "global.h"
#include "highlighter.h"

class MySubmissions : public QWidget
{
    Q_OBJECT
    QTableWidget *table;
    QFile *userSubmissionFile;
    QTextStream *stream;

    struct Submission
    {
        QString name, resu, date, num;
        qint16 isForFirstTestOnly;
        Submission () = default;
        Submission (const QString &num,
                    const QString &nam,
                    const QString &res,
                    const QString &dat,
                    const qint8 & forFirstTest):
            num(num), name(nam), resu (res), date(dat), isForFirstTestOnly(forFirstTest) {}
    };

    QMap <qint64, Submission> submissionsData;
    QMap <qint64, int> rowOfSubmission;

    QList <QPair<qint64, QString>> updatingQueue;
    QList <qint64> inQueue;
    QTimer *inQueueUpdater;
    QVBoxLayout *layout;
    QLabel *filterByProblemLabel;
    QLineEdit *filterTaskNumber;
    QPushButton *filterButton, *resetFilterButton;
    QHBoxLayout *filterByProblemLayout;
public:
    explicit MySubmissions(QWidget *parent = 0);
    ~MySubmissions();
signals:
    void taskNameRequest (const qint64 &, const QString &);
private slots:
    void getFilterTaskNumber ();
public slots:
    void emitter ();
    void processSubmission (const qint64 &id, const QString &taskName, const QString &, bool);
    void checkQueue ();
    void onTableCellClicked (int, int);
    void displayByProblem (const QString &);
    void displayByProblem();
    void getTaskName (const qint64 &, const QString &);
};

#endif // MYSUBMISSIONS_H
