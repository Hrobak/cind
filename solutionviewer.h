#ifndef SOLUTIONVIEWER_H
#define SOLUTIONVIEWER_H

#include <QWidget>
#include "global.h"
#include "highlighter.h"

class SolutionViewer : public QDialog
{
    Q_OBJECT
    QPlainTextEdit *edit;
    Highlighter *highlighter;
    QVBoxLayout *layout;
public:
    explicit SolutionViewer(const QString &text, QWidget *parent = 0);
    ~SolutionViewer ();
    
};

#endif // SOLUTIONVIEWER_H
