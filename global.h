#ifndef GLOBAL_H
#define GLOBAL_H

#include <QWidget>
#include <QNetworkReply>
#include <QLabel>
#include <QTimer>
#include <QTextCodec>
#include <QLineEdit>
#include <QPushButton>
#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QNetworkAccessManager>
#include <QString>
#include <QNetworkRequest>
#include <QMessageBox>
#include <QDebug>
#include <QUrl>
#include <QTimer>
#include <QNetworkCookieJar>
#include <QIODevice>
#include <QFile>
#include <QFileDialog>
#include <QTableWidget>
#include <QSignalMapper>
#include <QHeaderView>
#include <QProgressBar>
#include <QComboBox>
#include <QTableWidgetItem>
#include <QMap>
#include <QTextStream>
#include <QDataStream>
#include <QMutableListIterator>
#include <QEventLoop>
#include <QList>
#include <QPixmap>
#include <QIcon>
#include <QPlainTextEdit>

class GlobalManager final
{
    static QNetworkAccessManager * netw;
    static QString currentLogin;
    static QTextCodec *codec;
    friend class SendSolution;
    friend class Viewer;
    friend class Widget;
    friend class GoToTask;
    friend class TaskList;
    friend class MySubmissions;
public:
    GlobalManager () = delete;
    GlobalManager (const GlobalManager &) = delete;

    //returns 2 if connection error occured
    //returns 1 if there are 1 or more mathes
    //returns 0 otherwise
    int static checkResponse (QNetworkReply * repl, const QString &s)
    {

        if (repl->error() != QNetworkReply::NoError)
            return 2;
        QByteArray dat = repl->readAll();
        QString temp = codec->toUnicode(dat.data());
        if (temp.contains(s))
            return 1;
        return 0;
    }

    //Cutting GET-responses from server's submissions page
    void static cutResponse (QString &ss)
    {
        int indexTableBegins = ss.indexOf("<tr><td al");

        ss = ss.remove(0,indexTableBegins);
        //qDebug () << ss << "\n\n";

        int indexTableEnds = ss.indexOf("</td></table");
        ss = ss.remove(indexTableEnds,ss.length()-indexTableEnds);
        //qDebug () << ss << "\n\n";
        //TODO: Add code to handle nicks <inside>.
        ss.replace(QRegExp("<[^>]*>"),QString("\n"));
        ss.replace(QRegExp("(\n)+"),QString("\n"));
        //qDebug () << ss << "\n";
    }
};
#endif //GLOBAL_H
